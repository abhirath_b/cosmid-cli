import pycurl
from io import BytesIO
import sys
import threading
import time
import argparse
import os


#Config Variables
DOMAIN = 'http://52.55.225.65'
DOWNLOAD_LINK = '%s/cosmidOutput/summary/excel/query/'%DOMAIN
SIGNAL = False
ERROR = ''
##

def create_query():
    '''
    Make a call to newCosmitQueryId to generate a new ID for this query
    '''
    temp = BytesIO()
    
    c = pycurl.Curl()
    c.setopt(c.URL, '%s/newCosmidQueryId'%DOMAIN)
    c.setopt(c.WRITEDATA, temp)
    c.perform()
    c.close()
    
    qid = temp.getvalue()
    qid = int(str(qid).split(':')[1].split('}')[0])
    return qid

def set_primer_options(primer):
    opts = {}
    opts['primer_design']='true'
    if primer=='TIDE':
        opts['minSeparationUncleavedToCleaved']='200'
        opts['minCleavageProductSizeDifference']='0'
        opts['minAmpliconLength']='450'
        opts['maxAmpliconLength']='700'
        opts['optimalAmpliconLength']='600'
    elif primer=='Default':
        opts['minSeparationUncleavedToCleaved']='110'
        opts['minCleavageProductSizeDifference']='0'
        opts['minAmpliconLength']='220'
        opts['maxAmpliconLength']='330'
        opts['optimalAmpliconLength']='275'
    elif primer=='Illumina_250':
        opts['minSeparationUncleavedToCleaved']='62'
        opts['minCleavageProductSizeDifference']='0'
        opts['minAmpliconLength']='125'
        opts['maxAmpliconLength']='175'
        opts['optimalAmpliconLength']='160'
    elif primer=='Illumina_250_paired':
        opts['minSeparationUncleavedToCleaved']='120'
        opts['minCleavageProductSizeDifference']='0'
        opts['minAmpliconLength']='250'
        opts['maxAmpliconLength']='350'
        opts['optimalAmpliconLength']='300'
    elif primer=='SMRT':
        opts['minSeparationUncleavedToCleaved']='135'
        opts['minCleavageProductSizeDifference']='0'
        opts['minAmpliconLength']='250'
        opts['maxAmpliconLength']='600'
        opts['optimalAmpliconLength']='280'
    elif primer=='enzyme':
        opts['minSeparationUncleavedToCleaved']='150'
        opts['minCleavageProductSizeDifference']='125'
        opts['minAmpliconLength']='250'
        opts['maxAmpliconLength']='1000'
        opts['optimalAmpliconLength']='450'
    elif primer=='false':
        opts['minSeparationUncleavedToCleaved']='150'
        opts['minCleavageProductSizeDifference']='125'
        opts['minAmpliconLength']='250'
        opts['maxAmpliconLength']='1000'
        opts['optimalAmpliconLength']='450'
    return opts


def call_cosmid(queryID,args):
    import re    
    # Give comma seperated tags
    # set PAM sequence
    # the mismatch params all set to their max vals
    temp = BytesIO()
    status = BytesIO()
    # First, get new ID
    global SIGNAL,ERROR
    
    # Now begin the cosmid command
    cosmidArgs = {}
    cosmidArgs['queryId'] = queryID
    cosmidArgs['tag'] = args.guides
    cosmidArgs['hitlimit'] = '0'
    cosmidArgs['target_database']= args.target_db
    cosmidArgs['mismatch_no_indel']= args.no_indels
    cosmidArgs['mismatch_1_del']= args.one_base_del
    cosmidArgs['mismatch_1_ins']= args.one_base_ins
    cosmidArgs['tag_suffix']= args.pam
    cosmidArgs['tag_type'] = 'seq'
    cosmidArgs['timeout'] = '1000000'
    cosmidArgs['CheckBox_no_indel'] = 'true'
    cosmidArgs['CheckBox_1_del'] = 'true'
    cosmidArgs['CheckBox_1_ins'] = 'true'
    cosmidArgs['primer_design'] = args.primer_design if args.primer_design=='false' else 'true'
    primer_opts=set_primer_options(args.primer_design)
    cosmidArgs['minSeparationUncleavedToCleaved'] = primer_opts['minSeparationUncleavedToCleaved'] 
    cosmidArgs['minCleavageProductSizeDifference'] =  primer_opts['minCleavageProductSizeDifference'] 
    cosmidArgs['minAmpliconLength'] = primer_opts['minAmpliconLength']
    cosmidArgs['maxAmpliconLength'] = primer_opts['maxAmpliconLength']
    cosmidArgs['optimalAmpliconLength'] = primer_opts['optimalAmpliconLength']
    cosmidArgString = ''
    for i in cosmidArgs.keys():
        cosmidArgString += '%s=%s&'%(i,cosmidArgs[i])
    
    cosmidResponseUrl = '%s/cosmidresponse?%s'%(DOMAIN,cosmidArgString)
    
    c = pycurl.Curl()
    c.setopt(c.URL, cosmidResponseUrl)
    c.setopt(c.WRITEDATA, temp)
    c.perform()
    err = c.errstr()
    c.close()
    
    success = re.search(r'success',str(temp.getvalue()))
    if success:
        SIGNAL=True
    else:
        SIGNAL=False
        ERROR = err
        print("Processing failed. Please share the following error with the admins")
        print(err)


def check_status(queryID,timeout,stop):
    status = BytesIO()
    # First, get new ID
    import time
    DOMAIN = 'http://52.55.225.65'
    c = pycurl.Curl()
    
    while not stop():
        c.setopt(c.URL, '%s/cosmidstatus/%s'%(DOMAIN,queryID))
        c.setopt(c.WRITEDATA, status)
        c.perform()
        sys.stdout.write('\r%s'%status.getvalue())
        
        time.sleep(timeout)
    c.close()

        

def main():

    # Decalre and use commandline arguments 
    parser = argparse.ArgumentParser(description='Call AutoCosmid to find offtargets')
    parser.add_argument('--guides', metavar='File', help='Path to file containing guide sequences. Should contain one sequence in each line',default="guides.txt")
    parser.add_argument('--pam', metavar='PAM-sequence', help='PAM sequence to be used for Off Target Search',default="NRG")
    parser.add_argument('--target-db', metavar='name', help='Name of Target Database',default="hg38",choices=['hg38','hg38y','hg19'],type=str)
    parser.add_argument('--no-indels', metavar='N', help='Num of Indels',default=3,type=int,choices=[0,1,2,3])
    parser.add_argument('--one-base-del', metavar='N',help='Num of 1-Base Dels',default=2,type=int,choices=[0,1,2])
    parser.add_argument('--one-base-ins', metavar='N', help='Num of 1-Base Ins',default=2,type=int,choices=[0,1,2])
    parser.add_argument('--primer-design',metavar='opt',help='PCR Primer design options',default='false',choices=['false','Default','TIDE','Illumina_250','Illumina_250_paired','SMRT','enzyme'])

    args = parser.parse_args()
    
    
    print("Reading all input")

    try:
        guides = open(args.guides).read().split('\n')
        tags = ''
        for i in guides:
            if i!='':
                tags += i+','
        tags = tags[:-1]
        args.guides = tags

    except:
        print("Error processing guides. Please ensure you're following the prescribed format")
        sys.exit(0)
    
    print("Beginning run with the following options:")
    sys.stderr.write(str(args)+'\n')
    try:
        qId = create_query()
    except:
        print("Error initiating new query with autocosmid")
        sys.exit(0)
    print("New Query initiated with AutoCosmid")
    print("The QuerID is:%d"%qId)
    try:
        thread = threading.Thread(target=call_cosmid,args=(qId,args))
        thread.start()
    except:
        print("Error in initiating compute thread")
        sys.exit(0)
    print("Processing has begun")
    count = 0 
    while thread.is_alive():
        sys.stdout.write('               ')
        dots = '.'*(count%5)
        sys.stdout.write('\rProcessing'+dots)
        count+=1
        time.sleep(2)
    thread.join()
    print('')
    print("Processing complete")
    if SIGNAL==False:
        print("There has been an error. Terminating!")
        sys.exit(0)

    print("Download result...")
    
    import wget 
    filename=wget.download("%s/%s"%(DOWNLOAD_LINK,qId))
    
    os.mkdir("Query_%d"%qId)
    os.rename("./%s"%filename,"./Query_%d/%s"%(qId,filename))
    f = open("Query_%d/log"%qId,"w")
    f.write("Options used for this run are:\n")
    for key in vars(args):
        f.write("%s\t%s\n"%(key,vars(args)[key]))
    f.close()
    print("Result stored in file:Query_%d/%s"%(qId,filename))
    print("Result available at:%s/%d"%(DOWNLOAD_LINK,qId))
    print("Done!")

if __name__=="__main__":
    main()
